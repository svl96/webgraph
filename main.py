from multiprocessing import Process, Queue, Manager, RLock
from urllib import request, parse
import re
from typing import AnyStr, List


def get_title(data):
    title = re.search(r'<title>(.+?)</title>', data)
    return title[1]


def get_next_links(link: AnyStr) -> List[AnyStr]:
    ref_reg = re.compile(r'<a href=\"(.+?)\"')
    domain = list(parse.urlsplit(link))[:2] + ['']*3

    with request.urlopen(link) as f:
        data = f.read().decode("utf-8")

    links: List[AnyStr] = []
    for el in ref_reg.finditer(data):
        parsed = list(parse.urlsplit(el[1]))
        if parsed[1] == '':
            parsed = domain[:2] + parsed[2:]
        new_link = parse.urlunsplit(parsed)
        if new_link[:4] == 'http':
            links.append(new_link)

    return links


def save_to_file(link: AnyStr):
    with request.urlopen(link) as w:
        data = w.read().decode('utf-8')
        title = get_title(data)
        with open("pages\{}.html".format(title), "w",  encoding='utf-8') as f:
            f.write(data)


def bfs(queue: Queue, visited_dict, id, lock):
    while True:
        value = queue.get()
        save_to_file(value)
        nexts = get_next_links(value)
        for next_val in nexts:
            if next_val not in visited_dict:
                queue.put(next_val)
                visited_dict[next_val] = 1


def main():
    link = "https://en.wikipedia.org"

    workers = []
    queue = Queue()
    lock = RLock()
    queue.put(link)
    dict = Manager().dict()
    for i in range(10):
        worker = Process(target=bfs, args=(queue, dict, i, lock))
        workers.append(worker)
        worker.start()
    for w in workers:
        w.join()


if __name__ == '__main__':
    main()
